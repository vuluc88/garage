# Garage

## Checkout the code
```
cd existing_repo
git remote add origin https://gitlab.com/vuluc88/garage.git
git branch -M main
git push -uf origin main
```

## Setup
prerequisite: python3 and pip installed
```
cd garage
pip install virtualenv
virtualenv venv
./venv/scripts/activate
pip install -r requirements.txt
```

Migrating
```
py manage.py migrate
```

Create admin user for using admin page
```
py manage.py createsuperuser
```

## Runserver
don’t use this server in anything resembling a production environment. It’s intended only for use while developing
```
py manage.py runserver
```

## Run unit test
```
py manage.py test
```

## Simple UI
Admin page:
```
http://localhost:8000/admin
```

Simple user UI:
```
http://localhost:8000/parking/
```

Select Add new entry button to add new entry into parking. It will redirect to this page (with 1 is the garage_id)
```
http://localhost:8000/parking/garage/1/add-new-entry
```
Select "Parking Entries" button will bring you to below page, which is to view list of current accepted entries, which
is equal to car/bike currently parking in the garage
```
http://localhost:8000/parking/garage/1/entries
```
You can use above page to mark the exiting vehicle too
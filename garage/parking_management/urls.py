from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('garage/<int:garage_id>/add-new-entry', views.new_entry, name='new_parking_entry'),
    path('garage/<int:garage_id>/entries', views.view_entries, name='view_parking_entries'),
    path('garage/<int:garage_id>/accepted_entries', views.get_garage_accepted_entries, name='get_parking_entries'),
    path('entry/<int:entry_id>/exit', views.entry_exit_garage, name='exit_parking_entry'),
]

from django.contrib import admin

from .models import Garage, ParkingSlot, Entry

class ParkingSlotAdmin(admin.ModelAdmin):
    list_display = ('level', 'vehicle_type', 'availability')
    list_filter = ('garage__name', 'availability')

class EntryAdmin(admin.ModelAdmin):
    list_display = ('license_plate', 'vehicle_type', 'created_at', 'status')
    list_filter = ('garage__name',)

admin.site.register(Garage)
admin.site.register(ParkingSlot, ParkingSlotAdmin)
admin.site.register(Entry, EntryAdmin)
from django.http import HttpResponse
from django.shortcuts import render
from django.http import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_http_methods
from django.views.decorators.csrf import csrf_exempt
from .models import Garage, ParkingSlot, Entry, MOTORBIKE_TYPE, CAR_TYPE


def index(request):
    garage_lst = Garage.objects.all()
    context = {
        'garage_lst': garage_lst
    }
    return render(request, 'index.html', context)


def view_entries(request, garage_id):
    garage = Garage.objects.get(pk=garage_id)

    context = {
        'garage': garage
    }
    return render(request, 'garage_entries.html', context)


@csrf_exempt
@require_http_methods(["GET"])
def get_garage_accepted_entries(request, garage_id):
    garage = Garage.objects.get(pk=garage_id)
    motor_available_slots = ParkingSlot.objects.filter(
                garage=garage,
                vehicle_type=MOTORBIKE_TYPE,
                availability=True).count()
    car_available_slots = ParkingSlot.objects.filter(
                garage=garage,
                vehicle_type=CAR_TYPE,
                availability=True).count()
    entries = Entry.objects.filter(
                garage__id=garage_id,
                status='A').order_by('slot_occupation__level', 'vehicle_type', 'created_at')
    entries_lst = []
    for e in entries:
        entries_lst.append({
            'id': e.id,
            'level': e.slot_occupation.level,
            'license_plate': e.license_plate,
            'vehicle_type': e.get_vehicle_type_display()
        })
    return JsonResponse({'entries_list': entries_lst,
                         'motor_available_slot': motor_available_slots,
                         'car_available_slot': car_available_slots})


@csrf_exempt
@require_http_methods(["POST"])
def entry_exit_garage(request, entry_id):
    try:
        entry = Entry.objects.get(pk=entry_id)
    except Entry.DoesNotExist:
        return HttpResponseBadRequest(f'Entry id: {entry_id} does not exist!')
    entry.leave_parking()
    entry.save()
    return JsonResponse({'status': 'OK'})


def new_entry(request, garage_id):
    garage = Garage.objects.get(pk=garage_id)
    error = ""
    if request.method == 'POST':
        license_plate = request.POST.get('license_plate')
        vehicle_type = request.POST.get('vehicle_type')
        entry = Entry(
                    garage=garage,
                    vehicle_type=vehicle_type,
                    license_plate=license_plate,
                    status='W'
                )
        entry.save()
        try: 
            available_slot = ParkingSlot.objects.filter(
                                garage=garage,
                                vehicle_type=vehicle_type,
                                availability=True).order_by('level').first()
            entry.assign_slot(available_slot)
            entry.save()
        except Exception as e:
            print(e)
            entry.reject()
            entry.save()
            error = "Entry is rejected, parking slots are full."

    motor_available_slots = ParkingSlot.objects.filter(
                garage=garage,
                vehicle_type=MOTORBIKE_TYPE,
                availability=True).count()
    car_available_slots = ParkingSlot.objects.filter(
                garage=garage,
                vehicle_type=CAR_TYPE,
                availability=True).count()

    context = {
        'motor_available_slots': motor_available_slots,
        'car_available_slots': car_available_slots,
        'garage': garage,
        'error': error
    }
    return render(request, 'new_entry.html', context)

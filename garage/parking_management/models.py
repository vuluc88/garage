from django.db import models
from datetime import date, datetime

MOTORBIKE_TYPE = 'M'
CAR_TYPE = 'C'

VEHICLE_TYPES = (
    (MOTORBIKE_TYPE, 'Motorbike'),
    (CAR_TYPE, 'Car'),
)


class Garage(models.Model):
    name = models.CharField(max_length=200, verbose_name="Garage Name")
    address = models.CharField(max_length=255)
    city = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class ParkingSlot(models.Model):
    garage = models.ForeignKey(Garage, on_delete=models.CASCADE)
    level = models.IntegerField(default=1)
    vehicle_type = models.CharField(max_length=1, choices=VEHICLE_TYPES)
    availability = models.BooleanField(default=True)

    def __str__(self):
        return f'{self.garage} Level: {self.level} {self.vehicle_type}'


class Entry(models.Model):
    STATUSES = (
        ('W', 'Waiting'),
        ('A', 'Accepted'),
        ('R', 'Rejected'),
        ('L', 'Left'),
    )
    garage = models.ForeignKey(Garage, on_delete=models.CASCADE)
    vehicle_type = models.CharField(max_length=1, choices=VEHICLE_TYPES)
    license_plate = models.CharField(max_length=20)
    status = models.CharField(max_length=1, choices=STATUSES)
    slot_occupation = models.ForeignKey(ParkingSlot, on_delete=models.SET_NULL, default=None,
                                        blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    in_time = models.DateTimeField(default=None, blank=True, null=True)
    out_time = models.DateTimeField(default=None, blank=True, null=True)

    def __str__(self):
        return f'{self.license_plate} | Time: {self.created_at.strftime("%m/%d/%Y, %H:%M:%S")}'

    def reject(self):
        self.status = 'R'
        return True

    def assign_slot(self, slot):
        self.status = 'A'
        self.slot_occupation = slot
        self.in_time = datetime.now()
        print(slot)
        slot.availability = False
        slot.save()
        print(slot)
        return True

    def leave_parking(self):
        self.status = 'L'
        self.out_time = datetime.now()
        if self.slot_occupation:
            self.slot_occupation.availability = True
            self.slot_occupation.save()
        return True

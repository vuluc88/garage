from django.test import TestCase
from .models import *


class ParkingModelTestCase(TestCase):
    def setUp(self):
        g = Garage.objects.create(name="garage01")
        ParkingSlot.objects.create(garage=g, level=1, vehicle_type=MOTORBIKE_TYPE)
        ParkingSlot.objects.create(garage=g, level=2, vehicle_type=CAR_TYPE)

    def test_motorbike_parking(self):
        g = Garage.objects.get(name="garage01")
        new_entry = Entry.objects.create(garage=g, vehicle_type=MOTORBIKE_TYPE,
                                         license_plate='M1')
        bike_available_slot = ParkingSlot.objects.filter(garage=g,
                                                         vehicle_type=MOTORBIKE_TYPE,
                                                         availability=True).first()
        self.assertIsNotNone(bike_available_slot)

        new_entry.assign_slot(bike_available_slot)
        self.assertEqual(bike_available_slot.availability, False)
        self.assertEqual(new_entry.slot_occupation, bike_available_slot)
        bike_available_slot_count = ParkingSlot.objects.filter(
            garage=g, vehicle_type=MOTORBIKE_TYPE, availability=True).count()
        self.assertEqual(bike_available_slot_count, 0)

    def test_car_parking(self):
        g = Garage.objects.get(name="garage01")
        new_entry = Entry.objects.create(garage=g, vehicle_type=CAR_TYPE,
                                         license_plate='Car1')
        car_available_slot = ParkingSlot.objects.filter(garage=g,
                                                        vehicle_type=CAR_TYPE,
                                                        availability=True).first()
        self.assertIsNotNone(car_available_slot)

        new_entry.assign_slot(car_available_slot)
        self.assertEqual(car_available_slot.availability, False)
        self.assertEqual(new_entry.slot_occupation, car_available_slot)
        car_available_slot_count = ParkingSlot.objects.filter(
            garage=g, vehicle_type=CAR_TYPE, availability=True).count()
        self.assertEqual(car_available_slot_count, 0)

    def test_reject_parking(self):
        g = Garage.objects.get(name="garage01")
        new_entry = Entry.objects.create(garage=g, vehicle_type=CAR_TYPE,
                                         license_plate='Car2')
        new_entry.reject()
        self.assertEqual(new_entry.status, 'R')

    def test_leave_parking(self):
        g = Garage.objects.get(name="garage01")
        new_entry = Entry.objects.create(garage=g, vehicle_type=CAR_TYPE,
                                         license_plate='Car1')
        car_available_slot = ParkingSlot.objects.filter(garage=g,
                                                        vehicle_type=CAR_TYPE,
                                                        availability=True).first()
        new_entry.assign_slot(car_available_slot)
        car_available_slot_count = ParkingSlot.objects.filter(
            garage=g, vehicle_type=CAR_TYPE, availability=True).count()
        self.assertEqual(car_available_slot_count, 0)

        new_entry.leave_parking()
        self.assertEqual(new_entry.status, 'L')
        car_available_slot_count = ParkingSlot.objects.filter(
            garage=g, vehicle_type=CAR_TYPE, availability=True).count()
        self.assertEqual(car_available_slot_count, 1)
        slot = ParkingSlot.objects.get(pk=new_entry.slot_occupation.id)
        self.assertEqual(slot.availability, True)
